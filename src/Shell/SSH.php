<?php

declare(strict_types = 1);

namespace Brightfish\Utility\Shell;

use Brightfish\Utility\Shell;

class SSH extends Shell {

    public const REGEX_KEY_VALUE = '/^(?<key>[^ ]+)( (?<value>.+))?$/m';

    public const KNOWNHOSTS_DIR = '.phpshell-knownhosts';

    protected bool $_usePasswordAuth = FALSE;

    protected Shell\SSH\Host $_host;
    protected array $_options = [];

    /**
     * @param string $key
     *
     * @return void
     * @throws CommandException
     */
    public static function AddPrivateKeyFromString(string $key) : void {
        $shell = new Shell();
        $shell->runOrThrow('ssh-add -', FALSE, $key, 'Unable to add SSH private key to agent: ssh-add failed');
    }

    /**
     * @param string $host
     * @param string $key
     * @return string|null
     * @throws CommandException
     */
    public static function GetValueForSSHConfigKey(string $host, string $key) : ?string {

        $shell = new Shell();
        $config = $shell->runOrThrow('ssh -G ' . escapeshellarg($host));

        preg_match_all(self::REGEX_KEY_VALUE, $config['stdout'], $matches, PREG_SET_ORDER);

        foreach ($matches as $match) {

            if ($match['key'] === $key) {
                return $match['value'];
            }

        }

        return NULL;
    }

    /**
     * @param string|Shell\SSH\Host $host
     *
     * @return void
     * @throws CommandException
     * @throws Exception
     */
    public static function AddHostKeyIfNotKnown(string|Shell\SSH\Host $hostInput) : void {
        $shell = new Shell();
        if (is_string($hostInput)) {
            $hostObj = self::Host($hostInput);
        } else {
            $hostObj = $hostInput;
        }
        $host = $hostObj->getRootHost();
        $hostname = $host->hostname;
        $port = $host->port;

        $userInfo = posix_getpwuid(posix_getuid());
        $homedir = $userInfo['dir'];
        $knownHostsPath = $homedir . '/' . static::KNOWNHOSTS_DIR;
        $knownHostsFile = $knownHostsPath . '/' . $hostname . ':' . $port;

        if (!is_dir($knownHostsPath) && !mkdir($knownHostsPath, 0700)) {
            throw new Exception('Unable to create directory for known_hosts files: ' . $knownHostsPath);
        }

        if (is_file($knownHostsFile) && file_get_contents($knownHostsFile) != '') {
            if (!$shell->runForExitCode('ssh-keygen -f ' . escapeshellarg($knownHostsFile) . ' -F ' . escapeshellarg($hostname))) {
                throw new Exception('known_hosts file found at ' . $knownHostsFile . ', but no matching key for given host is present');
            }
        } else {
            $extraOpts = php_uname('s') == 'Darwin' ? '-H ' : '';
            $res = $shell->run('ssh-keyscan ' . $extraOpts . '-p ' . $port . ' -t rsa ' . escapeshellarg($hostname) . ' > ' . escapeshellarg($knownHostsFile));

            if ($res['exitcode'] != 0) {
                throw new Exception('Unable to add host key to known_hosts file ' . $knownHostsFile . "\n" . $res['stdout'] . "\n" . $res['stderr']);
            }

            $shell->runOrThrow('chmod 600 ' . escapeshellarg($knownHostsFile));
        }
    }

    public static function Host(string $hostname) : Shell\SSH\Host {
        return new Shell\SSH\Host($hostname);
    }

    public function __construct(string|Shell\SSH\Host $host, ?string $user = NULL, ?string $password = NULL, array $options = []) {
        $this->_host = is_string($host) ? new Shell\SSH\Host($host) : $host;
        $this->_options = $options;

        parent::__construct($user, $password);

        self::AddHostKeyIfNotKnown($this->_host);
    }

    /**
     * @param string $command
     * @param bool   $sudo
     *
     * @return void
     */
    protected function _openProcess(string $command, bool $sudo = FALSE) : void {
        if ($this->_user === NULL) {
            throw new \RuntimeException('Unable to open SSH connection: No user has been set!');
        }
        if ($sudo) {
            $command = $this->_sudoizeCommand($command);
        }
        $cmd = $this->_host->getCommandline($this->_user, $command, $this->_options);
        if ($this->_usePasswordAuth) {

            $cmd = 'sshpass -e ' . $cmd;

            $env = $this->_env;
            if ($env === NULL) {
                $env = getenv();
            }

            $env['SSHPASS'] = $this->_password;
            $this->_env = $env;

        }

        parent::_openProcess($cmd, FALSE);

        if ($sudo) {
            $this->_stdInWriteBuffer = $this->_password . "\n";
        }
    }

    /**
     * @param bool $usePasswordAuth
     *
     * @return void
     */
    public function setUsePasswordAuth(bool $usePasswordAuth) : void {

        if ($usePasswordAuth) {

            exec('which sshpass', $output, $retVar);
            if ($retVar !== 0) {
                throw new \RuntimeException('sshpass is not installed on your system, for MacOS systems use brew install http://git.io/sshpass.rb');
            }

        }

        $this->_usePasswordAuth = $usePasswordAuth;

    }

    /**
     * @param array $options Options to with using -o on the resulting ssh connection
     *
     * @return void
     */
    public function setOptions(array $options = []) : void {
        $this->_options = $options;
    }

}
