<?php

declare(strict_types = 1);

namespace Brightfish\Utility\Shell;

use Brightfish\Utility\Shell\Monitor\Check;

class Monitor extends SSH {

    private ?int $_lastExitCode = NULL;
    private ?string $_lastStdOut = NULL;
    private ?string $_lastStdErr = NULL;

    private ?string $_separator;

    /**
     * @var Check[]
     */
    private array $_checks = [];

    private ?\Closure $_updateCallback = NULL;

    /**
     * @param $callback
     *
     * @return void
     */
    public function setUpdateCallback(callable $callback) : void {
        $this->_updateCallback = $callback(...);
    }

    /**
     * @param Check $check
     *
     * @return void
     * @throws Exception
     */
    public function addCheck(Check $check) : void {
        if ($this->_process) {
            throw new Exception('Monitor shell already started');
        }

        $this->_checks[$check->key] = $check;
    }

    /**
     * @return Check[]
     */
    public function getAllChecks() : array {
        return $this->_checks;
    }

    /**
     * @param string $key
     *
     * @return Check|null
     */
    public function getCheck(string $key) : ?Check {
        return $this->_checks[$key] ?? NULL;
    }

    /**
     * @param string $key
     *
     * @return void
     * @throws Exception
     */
    public function removeCheck(string $key) : void {
        if ($this->_process) {
            throw new Exception('Monitor shell already started');
        }

        if (isset($this->_checks[$key])) {
            unset($this->_checks[$key]);
        }
    }

    /**
     * @return void
     * @throws Exception
     */
    public function startCheckProcess() : void {
        if ($this->_process) {
            throw new Exception('Monitor shell already started');
        }

        $this->_lastExitCode = NULL;
        $this->_lastStdOut = NULL;
        $this->_lastStdErr = NULL;

        $this->_separator = md5(time() . mt_rand());
        $cmd = "SEPARATOR={$this->_separator}; while true; do read -n 1 CONTINP; ";
        foreach ($this->_checks as $check) {
            $cmd .= $check->command . ' 2>&1; echo $? $SEPARATOR; ';
        }
        $cmd .= 'done';

        $this->_openProcess($cmd);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function readCheckStatus() : void {
        if ($this->_process === NULL) {
            throw new Exception('Monitor shell not yet started');
        }

        $status = $this->getProcessStatus();
        if (!$status['running']) {
            $this->_lastExitCode = $status['exitcode'];
            $this->_lastStdOut = fread($this->_pipes[1], 8192) ?: NULL;
            $this->_lastStdErr = fread($this->_pipes[2], 8192) ?: NULL;
            throw new Exception('Monitor shell was terminated prematurely (exitcode: ' . $this->_lastExitCode . "). Last output:\n\n" . $this->_lastStdOut . "\n\n" . $this->_lastStdErr);
        }

        fwrite($this->_pipes[0], "\n");
        fflush($this->_pipes[0]);

        $stdout = [];

        $sepCount = 0;

        while ($sepCount < count($this->_checks) && !(feof($this->_pipes[1]) && feof($this->_pipes[2]))) {
            if (($line = fgets($this->_pipes[1])) !== FALSE) {
                $stdout[] = $line;
                if (preg_match('/' . $this->_separator . '/', $line)) {
                    $sepCount++;
                }
            }
            fgets($this->_pipes[2]);
        }

        foreach ($this->_checks as $check) {
            $checkOutput = [];
            while (!preg_match('/(\d+) ' . $this->_separator . '/', $stdout[0], $matches)) {
                $checkOutput[] = array_shift($stdout);
            }
            $checkExitCode = $matches[1];
            array_shift($stdout);

            $check->processResult($checkOutput, (int) $checkExitCode);
        }

        $this->_updateCallback?->__invoke();
    }

    /**
     * @return void
     * @throws Exception
     */
    public function finishCheckProcess() : void {
        if ($this->_process === NULL) {
            throw new Exception('Monitor shell not yet started');
        }

        proc_terminate($this->_process);

        $this->_closeProcess();
    }

    /**
     * @param array $exclude
     *
     * @return bool
     * @throws Exception
     */
    public function allChecksOK(array $exclude = []) : bool {
        return $this->checksOK(array_keys($this->_checks));
    }

    /**
     * @param array $checks
     *
     * @return bool
     * @throws Exception
     */
    public function checksFailed(array $checks) : bool {
        if ($this->_process === NULL) {
            throw new Exception('Monitor shell not yet started');
        }

        $failed = TRUE;
        foreach ($this->_checks as $check) {
            if (in_array($check->key, $checks)) {
                if ($check->result) {
                    $failed = FALSE;
                    break;
                }
            }
        }

        return $failed;
    }

    /**
     * @param array $checks
     *
     * @return bool
     * @throws Exception
     */
    public function checksOK(array $checks) : bool {
        if ($this->_process === NULL) {
            throw new Exception('Monitor shell not yet started');
        }

        $isOK = TRUE;
        foreach ($this->_checks as $check) {
            if (in_array($check->key, $checks)) {
                if (!$check->result) {
                    $isOK = FALSE;
                    break;
                }
            }
        }

        return $isOK;
    }

    /**
     * @param $result
     *
     * @return array
     * @throws Exception
     */
    public function getChecksWithResult($result) : array {
        if ($this->_process === NULL) {
            throw new Exception('Monitor shell not yet started');
        }

        $checks = [];
        foreach ($this->_checks as $check) {
            if ($check->result == $result) {
                $checks[] = $check->key;
            }
        }

        return $checks;
    }
}
