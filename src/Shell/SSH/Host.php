<?php

declare(strict_types = 1);

namespace Brightfish\Utility\Shell\SSH;

use Brightfish\Utility\Shell\SSH;

class Host {

    public readonly string $hostname;
    public readonly int $port;
    public readonly ?Host $forwardedByHost;
    public readonly ?string $privateKeyPath;
    public readonly ?string $forceUsername;
    protected ?int $_connectionTimeout = NULL;

    public function __construct(string $hostname, Host $forwardedBy = NULL, ?string $privateKeyPath = NULL, ?string $forceUsername = NULL) {
        $addrParts = explode(':', $hostname);

        $addr = $addrParts[0];

        $port = (int) ($addrParts[1] ?? 22);

        $this->hostname = $addr;
        $this->port = $port;
        $this->forwardedByHost = $forwardedBy;
        $this->privateKeyPath = $privateKeyPath;
        $this->forceUsername = $forceUsername;
    }

    public function setConnectionTimeout(int $timeout = NULL) : void {
        $this->_connectionTimeout = $timeout;
    }

    public function forward($hostname) : Host {
        return new Host($hostname, $this, $this->privateKeyPath);
    }

    public function getCommandline(string $user, string $command, array $options = []) : string {
        $userInfo = posix_getpwuid(posix_getuid());
        $homedir = $userInfo['dir'];

        $unsafeKnownHostsBypassOptions = ['StrictHostKeyChecking=no', 'LogLevel=ERROR', 'UserKnownHostsFile=/dev/null'];
        $cur = $this;
        do {

            $localOptions = $options;

            $outerCmd = 'ssh';
            if ($cur->forwardedByHost) {
                $localOptions = array_merge($localOptions, $unsafeKnownHostsBypassOptions);
            } else {
                if ($cur->privateKeyPath) {
                    $outerCmd .= ' -i ' . escapeshellarg($cur->privateKeyPath);
                }
                $knownHostsPath = $homedir . '/' . SSH::KNOWNHOSTS_DIR;
                $knownHostsFile = $knownHostsPath . '/' . $cur->hostname . ':' . $cur->port;

                $localOptions = array_merge($localOptions, ['UserKnownHostsFile=' . $knownHostsFile]);

            }

            foreach ($localOptions as $option) {
                $outerCmd .= ' -o ' . escapeshellarg($option);
            }

            $outerCmd .= ' -p ' . $cur->port;
            $outerCmd .= ' -A';
            if ($this->_connectionTimeout !== NULL) {
                $outerCmd .= ' -o ConnectTimeout=' . $this->_connectionTimeout;
            }
            $effectiveUser = $cur->forceUsername ? $cur->forceUsername : $user;
            $outerCmd .= ' ' . escapeshellarg($effectiveUser . '@' . $cur->hostname);
            $outerCmd .= ' ' . escapeshellarg($command);

            $command = $outerCmd;
        } while ($cur = $cur->forwardedByHost);

        return $command;
    }

    public function getRootHostname() : string {
        return $this->getRootHost()->hostname;
    }

    public function getRootHost() : Host {
        if ($this->forwardedByHost !== NULL) {
            return $this->forwardedByHost->getRootHost();
        }
        return $this;
    }

    public function __toString() : string {
        return $this->hostname;
    }
}
