<?php

declare(strict_types = 1);

namespace Brightfish\Utility\Shell\SSH;

use Brightfish\Utility\Shell\ConnectionException;
use Brightfish\Utility\Shell\Exception;
use Brightfish\Utility\Shell\SSH;
use Brightfish\Utility\Shell\WritableStream;

class Interactive extends SSH {

    public static string $DefaultShell = 'PS1="" /bin/bash --noprofile --norc';

    protected string $_shell;
    protected bool $_elevated = FALSE;

    public ?string $label = NULL;

    /**
     * @var int|null
     */
    protected $_lastExitCode = NULL;

    public function __construct($host, string $user = NULL, string $password = NULL, string $shell = NULL, array $options = []) {
        parent::__construct($host, $user, $password, $options);
        $this->_shell = $shell ?? static::$DefaultShell;
        $this->_options = $options;
    }

    public function __destruct() {
        if ($this->isRunning()) {
            $this->disconnect();
        }
    }

    public function connect() : void {
        $this->_assertConnectionActive(FALSE);

        $this->start($this->_shell);

        $this->_assertShellHealth();
    }

    public function disconnect() : void {
        $this->_assertConnectionActive();

        $this->end();
    }

    public function elevate() : void {
        $this->_assertConnectionActive();

        if ($this->_elevated) {
            throw new Exception('Shell is already elevated!');
        }

        if (!$this->testPassword()) {
            throw new Exception('Password is not valid, can\'t elevate shell');
        }

        $this->writeStdIn($this->_sudoizeCommand('/bin/bash -c ' . escapeshellarg(self::$DefaultShell)) . "\n" . $this->_password . "\n");

        do {
            $this->update();
        } while ($this->getStdInBufferFill() > 0);

        $res = $this->run('whoami');
        if (! ($res['exitcode'] == 0 && trim($res['stdout']) == 'root')) {
            var_dump($res);
            throw new Exception('Elevation to root user failed: ' . $res['stderr']);
        }

        $this->_elevated = TRUE;
    }

    public function isElevated() : bool {
        return $this->_elevated;
    }

    private function _assertShellHealth() : void {
        $verified = FALSE;
        $connectionFailure = FALSE;
        $rawStdErr = '';

        if ($this->isRunning()) {
            $this->writeStdIn("echo OK\n");

            while ($this->update()) {
                $rawStdOut = $this->readStdOut();
                $rawStdErr = $this->readStdErr();

                if (preg_match('/^OK' . "\n" . '$/', $rawStdOut)) {
                    $verified = TRUE;
                    break;
                }
            }

            if (!$this->isRunning() && !$verified) {
                if ($this->getExitCode() == 255) {
                    $connectionFailure = TRUE;
                }
            }
        }

        if (!$verified) {
            $error = trim($rawStdErr);
            if ($connectionFailure) {
                throw new ConnectionException($error);
            } else {
                throw new Exception("Shell health could not be asserted: $error");
            }
        }
        $this->readStdOut(TRUE);
        $this->readStdErr(TRUE);
    }

    public function run(string $cmd, bool $sudo = FALSE, string $stdin = '', ?WritableStream $stdOutStream = NULL, ?WritableStream $stdErrStream = NULL) : array {
        $this->_assertConnectionActive();

        $anchor = md5(time() . mt_rand());

        if ($sudo && !$this->_elevated) {
            $cmd = $this->_sudoizeCommand($cmd);
            $stdin = $this->_password . "\n" . $stdin;
        }

        if (strlen($stdin)) {
            $cmd = 'head -c ' . strlen($stdin) . ' - | ' . $cmd;
        }

        $this->writeStdIn($cmd . "; echo ===\\(\$?\\)$anchor===\n" . $stdin);

        $exitCode = NULL;
        $stdOut = NULL;
        $stdErr = NULL;

        $previousStdOutOffset = 0;
        $previousStdErrOffset = 0;

        while ($this->update()) {

            $rawStdOut = $this->readStdOut();
            $rawStdErr = $this->readStdErr();

            $shouldExit = false;

            if (preg_match('/===\((\d+)\)' . $anchor . '===' . "\n" . '$/', $rawStdOut, $match)) {
                $exitCode = (int) $match[1];
                $stdOut = substr($this->readStdOut(TRUE), 0, strlen($rawStdOut) - strlen($match[0]));
                $stdErr = $this->readStdErr(TRUE);
                $rawStdOut = $stdOut;
                $rawStdErr = $stdErr;
                $shouldExit = true;
            }

            if ($stdOutStream) {
                fwrite($stdOutStream->getStream(), substr($rawStdOut, $previousStdOutOffset));
            }
            if ($stdErrStream) {
                fwrite($stdErrStream->getStream(), substr($rawStdErr, $previousStdErrOffset));
            }

            if ($shouldExit) {
                break;
            }

            $previousStdOutOffset = strlen($rawStdOut);
            $previousStdErrOffset = strlen($rawStdErr);

        }

        if ($exitCode === NULL) {
            throw new Exception('No anchor found in output: [stdout: ' . $rawStdOut . '] [stderr: ' . $rawStdErr . ']');
        }

        $this->_lastExitCode = $exitCode;

        return ['exitcode' => $exitCode, 'stdout' => $stdOut, 'stderr' => $stdErr];
    }

    /**
     * @return int|null
     */
    public function getExitCode() : ?int {
        return $this->_lastExitCode;
    }

    protected function _assertConnectionActive(bool $status = TRUE) : void {
        if ($status && !$this->isRunning()) {
            $this->end(FALSE);
            throw new Exception('Connection should be active but nothing is running. Ending process!');
        }
        if (!$status && $this->isRunning()) {
            throw new Exception('Connection should NOT be active but contains running process!');
        }
    }
}
