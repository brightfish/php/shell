<?php

declare(strict_types = 1);

namespace Brightfish\Utility\Shell\Monitor;

class Check {

    public string $key;
    public string $command;
    public ?\Closure $postProcessor;
    public int $exitCode;
    public array $output;
    public bool $result;

    public function __construct(string $key, string $command, callable $postProcessor = NULL) {
        $this->key = $key;
        $this->command = $command;
        $this->postProcessor = $postProcessor ? $postProcessor(...) : $postProcessor;
    }

    /**
     * @param array $output
     * @param int   $exitCode
     *
     * @return void
     */
    public function processResult(array $output, int $exitCode) : void {
        $this->exitCode = $exitCode;
        $this->output = $output;
        $this->result = ($this->postProcessor === NULL ? ($this->exitCode == 0) : $this->postProcessor->__invoke($output, $exitCode));
    }

}
