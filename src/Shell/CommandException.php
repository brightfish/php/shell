<?php

declare(strict_types = 1);

namespace Brightfish\Utility\Shell;

use Brightfish\Utility\Shell;

class CommandException extends Exception {

    public function __construct(string $message, Shell $shell) {
        parent::__construct($message . ' (exitcode: ' . $shell->getExitCode() . "):\n" . $shell->readStdErr(FALSE) . "\n");
    }

}
