<?php

declare(strict_types = 1);

namespace Brightfish\Utility\Shell;

class WritableStream {

    /**
     * @var resource
     */
    private $_stream;
    
    public function __construct($stream) {

        if (!is_resource($stream)) {
            throw new \InvalidArgumentException('Expected resource or NULL, got ' . gettype($stream));
        }

        $meta = stream_get_meta_data($stream);
        if (strpos($meta['mode'], 'w') === FALSE) {
            throw new \InvalidArgumentException('Resource has to be writable');
        }

        $this->_stream = $stream;

    }

    public function getStream() {
        return $this->_stream;
    }

}
