<?php

declare(strict_types = 1);

namespace Brightfish\Utility\Shell;

class Exception extends \Exception {

    public function __construct(string $message) {
        parent::__construct($message);
    }
}
