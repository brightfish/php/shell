<?php

declare(strict_types = 1);

namespace Brightfish\Utility;

use Brightfish\Utility\Shell\Exception;

class Shell {

    const DEFAULT_PIPE_BUFFER_SIZE = 81920;

    /* Current session */
    protected readonly ?string $_user;
    protected ?string $_password;

    /* Process properties */
    protected ?array $_env = NULL;

    /**
     * @var null|resource
     */
    protected $_process = NULL;
    protected ?array $_lastStatus = NULL;

    /**
     * @var resource[]
     */
    protected array $_pipes = [NULL, NULL, NULL];
    protected ?string $_stdInWriteBuffer = NULL;
    protected string $_stdOutReadBuffer = '';
    protected string $_stdErrReadBuffer = '';
    protected bool $_closeStdInAfterWrite = FALSE;

    /* Defaults for new sessions */
    protected static ?string $_Username = NULL;
    protected static ?string $_Password = NULL;


    /**
     * Set default username based on current process owner
     **/

    public static function SetCurrentUsername() : void {
        $userInfo = posix_getpwuid(posix_getuid());
        $user = $userInfo['name'];
        Shell::SetUsername($user);
    }

    public static function SetUsername(string $username) : void {
        Shell::$_Username = $username;
    }

    public static function SetPassword(string $password) : void {
        Shell::$_Password = $password;
    }

    public static function GetUsername() : ?string {
        return Shell::$_Username;
    }


    public static function PromptPassword(string $prompt = 'Enter password') : string {
        $command = "/usr/bin/env bash -c 'read -s -p \"" . addslashes($prompt . ': ') . "\" mypassword && echo \$mypassword'";
        $password = rtrim(shell_exec($command));
        echo "\n";
        return $password;
    }

    public static function AskPassword(string $prompt = 'Enter password') : void {
        Shell::$_Password = static::PromptPassword($prompt);
    }

    public static function StoreCredentials(string $path = NULL) : void {
        if (!Shell::$_Username || !Shell::$_Password) {
            throw new Exception("Can't store credentials: need both username and password");
        }

        $path = $path ?? ($_SERVER['HOME'] . '/.phpshellcredentials');
        $data = serialize(['username' => Shell::$_Username, 'password' => Shell::$_Password]);
        $pass = md5($_SERVER['HOME'] . $_SERVER['SHELL'] . $_SERVER['TERM']);
        $salt = substr(md5((string)mt_rand(), TRUE), 8);

        [$key, $iv] = self::_DeriveKeyAndInitializationVector($pass, $salt);

        $encryptedData = openssl_encrypt($data, 'aes-256-cbc', $key, OPENSSL_RAW_DATA, $iv);
        file_put_contents($path, base64_encode('Salted__' . $salt . $encryptedData));
        chmod($path, 0600);
    }

    public static function RestoreCredentials(string $path = NULL) : bool {
        $path = $path ?? ($_SERVER['HOME'] . '/.phpshellcredentials');
        if (!is_file($path)) {
            return FALSE;
        }

        $data = base64_decode(file_get_contents($path));
        $pass = md5($_SERVER['HOME'] . $_SERVER['SHELL'] . $_SERVER['TERM']);
        $salt = substr($data, 8, 8);

        [$key, $iv] = self::_DeriveKeyAndInitializationVector($pass, $salt);

        $decrypted = openssl_decrypt(substr($data, 16), 'aes-256-cbc', $key, OPENSSL_RAW_DATA, $iv);
        if (($struct = @unserialize($decrypted)) === FALSE) {
            return FALSE;
        }

        Shell::$_Username = $struct['username'];
        Shell::$_Password = $struct['password'];

        return TRUE;
    }

    private static function _DeriveKeyAndInitializationVector(string $pass, string $salt) : array {
        $ivLen = openssl_cipher_iv_length('aes-256-cbc');
        $keyLen = $ivLen * 2;

        $total_len = $keyLen + $ivLen;
        $salted = '';
        $dx = '';
        // Salt the key and iv
        while (strlen($salted) < $total_len) {
            $dx = md5($dx . $pass . $salt, TRUE);
            $salted .= $dx;
        }
        $key = substr($salted, 0, $keyLen);
        $iv = substr($salted, $keyLen, $ivLen);

        return [$key, $iv];
    }


    public function __construct(?string $user = NULL, string $password = NULL) {
        $this->_user = $user ?: Shell::$_Username;

        $this->_password = ($password === NULL ? Shell::$_Password : $password);
    }

    public function updatePassword(string $password) : void {
        $this->_password = $password;
    }

    public function testPassword() : bool {
        $ret = $this->run('true', TRUE);
        return $ret['exitcode'] == 0;
    }


    public function run(string $cmd, bool $sudo = FALSE, string $stdin = '') : array {
        $this->start($cmd, $sudo);

        $this->writeStdIn($stdin, TRUE);

        while ($this->update());

        $this->end(TRUE);

        return ['exitcode' => $this->getExitCode(), 'stdout' => $this->readStdOut(FALSE), 'stderr' => $this->readStdErr(FALSE)];
    }

    public function runForExitCode(string $cmd, bool $sudo = FALSE, string $stdin = '', int $expectedExitCode = 0) : bool {
        $res = $this->run($cmd, $sudo, $stdin);
        return ($res['exitcode'] == $expectedExitCode);
    }

    public function runForStdOut(string $cmd, bool $sudo = FALSE, string $stdin = '', int $expectedExitCode = NULL) : ?string {
        $res = $this->run($cmd, $sudo, $stdin);
        if ($expectedExitCode !== NULL && $res['exitcode'] != $expectedExitCode) {
            return NULL;
        }
        return $res['stdout'];
    }

    public function runForStdErr(string $cmd, bool $sudo = FALSE, string $stdin = '', int $expectedExitCode = NULL) : ?string {
        $res = $this->run($cmd, $sudo, $stdin);
        if ($expectedExitCode !== NULL && $res['exitcode'] != $expectedExitCode) {
            return NULL;
        }
        return $res['stderr'];
    }

    public function runOrThrow(string $cmd, bool $sudo = FALSE, string $stdin = '', string $exceptionMessage = NULL, int $expectedExitCode = 0) : array {
        $res = $this->run($cmd, $sudo, $stdin);
        if ($res['exitcode'] != $expectedExitCode) {
            $exceptionMessage = ($exceptionMessage ?: "Command '$cmd' failed with error: " . $res['stderr']);
            throw new Shell\CommandException($exceptionMessage, $this);
        }
        return $res;
    }


    public function start($cmd, $sudo = FALSE) : void {
        $this->_openProcess($cmd, $sudo);
    }

    public function end(bool $assert = TRUE) : void {
        $this->_closeProcess($assert);
    }

    public function isRunning() : bool {
        $status = $this->getProcessStatus();
        return (bool)($status['running'] ?? FALSE);
    }

    public function getExitCode() : ?int {
        if ($this->isRunning()) {
            return NULL;
        }
        return ($this->getProcessStatus()['exitcode'] ?? NULL);
    }

    public function writeStdIn(string $data, bool $closeAfterWrite = FALSE) : void {
        $this->_assertProcessActive();

        $this->_stdInWriteBuffer .= $data;
        if ($closeAfterWrite) {
            $this->_closeStdInAfterWrite = TRUE;
        }
    }

    public function getStdInBufferFill() : int {
        if ($this->_stdInWriteBuffer === NULL) {
            return 0;
        }
        return strlen($this->_stdInWriteBuffer);
    }

    public function closeStdInAfterWrite() : void {
        $this->_closeStdInAfterWrite = TRUE;
    }

    public function readStdOut(bool $truncate = FALSE) : string {
        $ret = $this->_stdOutReadBuffer;
        if ($truncate) {
            $this->_stdOutReadBuffer = '';
        }
        return $ret;
    }

    public function readStdErr(bool $truncate = FALSE) : string {
        $ret = $this->_stdErrReadBuffer;
        if ($truncate) {
            $this->_stdErrReadBuffer = '';
        }
        return $ret;
    }

    public function hasProcess() : bool {
        return ($this->_process !== NULL);
    }

    public function getProcessStatus() : ?array {
        if ($this->_process && ($this->_lastStatus === NULL || $this->_lastStatus['running'])) {
            $this->_lastStatus = proc_get_status($this->_process);
        }

        return $this->_lastStatus;
    }

    public function resetLastStatus() {
        $this->_lastStatus = NULL;
    }

    public function update(float $timeout = 0.2, callable $writeStdIn = NULL, callable $readStdOut = NULL, callable $readStdErr = NULL) : bool {
        $this->_assertProcessActive();

        $timeInt = (int)floor($timeout);
        $timePart = (int)($timeout * 1000000);

        $read = [];
        $write = [];
        $except = [];

        foreach ([$this->_pipes[1], $this->_pipes[2]] as $readPipe) {
            if (is_resource($readPipe)) {
                $read[] = $readPipe;
            }
        }

        if (($this->_stdInWriteBuffer !== NULL || $writeStdIn) && is_resource($this->_pipes[0])) {
            $write[] = $this->_pipes[0];
        }
        if (!count($read) && !count($write)) {
            return FALSE;
        }
        error_clear_last();
        if (FALSE === ($numChangedStreams = @stream_select($read, $write, $except, $timeInt, $timePart))) {
            $err = error_get_last();
            if ($err === NULL) {
                //select simply timed out
                return TRUE;
            }
            $matches = [];
            preg_match('/unable to select \[(?<code>\d+)\]: (?<message>.+)$/', $err['message'] ?? '', $matches);
            if ($matches['code'] == 4) {
                //"Interrupted system call, be silent
                return TRUE;
            } else {
                throw new \ErrorException($err['message'], $err['type'], E_ERROR, $err['file'], $err['line']);
            }
        } elseif ($numChangedStreams > 0) {

            foreach ($read as $readStream) {
                $data = '';
                do {
                    $chunk = fread($readStream, 8192);
                    if ($chunk === FALSE) {
                        throw new Exception('fread() failed');
                    }
                    $data .= $chunk;
                } while (strlen($chunk) != 0);

                $eof = FALSE;
                if (feof($readStream)) {
                    $eof = TRUE;
                    fclose($readStream);
                }

                if ($readStream == $this->_pipes[1]) {
                    if ($readStdOut) {
                        $readStdOut($data, $eof);
                    } else {
                        $this->_stdOutReadBuffer .= $data;
                    }
                }
                if ($readStream == $this->_pipes[2]) {
                    if ($readStdErr) {
                        $readStdErr($data, $eof);
                    } else {
                        $this->_stdErrReadBuffer .= $data;
                    }
                }
            }

            if (count($write)) {
                $writeStream = $write[0];
                if ($this->_stdInWriteBuffer !== NULL) {
                    $data = $this->_stdInWriteBuffer;
                } elseif ($writeStdIn !== NULL) {
                    $data = $writeStdIn();
                    if ($data === FALSE) {
                        $data = NULL;
                        $this->closeStdInAfterWrite();
                    }
                }

                if ($data !== NULL) {
                    $len = fwrite($writeStream, $data, min(strlen($data), 8192));
                    $this->_stdInWriteBuffer = ($len < strlen($data) ? substr_replace($data, '', 0, $len) : NULL);
                }
                if ($this->_stdInWriteBuffer === NULL && $this->_closeStdInAfterWrite) {
                    fclose($writeStream);
                    $this->_closeStdInAfterWrite = FALSE;
                }
            }
        }

        return TRUE;
    }

    public function writeStdOutToShell(Shell $writeShell, int $bufferSize = NULL, callable $progressFunction = NULL) : void {
        $this->_writePipeToShell(1, $writeShell, $bufferSize ?: self::DEFAULT_PIPE_BUFFER_SIZE, $progressFunction);
    }

    public function writeStdErrToShell(Shell $writeShell, int $bufferSize = NULL, callable $progressFunction = NULL) : void {
        $this->_writePipeToShell(2, $writeShell, $bufferSize ?: self::DEFAULT_PIPE_BUFFER_SIZE, $progressFunction);
    }

    private function _writePipeToShell(int $pipeNumber, Shell $writeShell, int $bufferSize, callable $progressFunction = NULL) : void {
        $readShell = $this;
        $readShell->closeStdInAfterWrite();

        $totalRead = $lastTotalRead = 0;

        while ($writeShell->update(0.2, function () use ($pipeNumber, $readShell, $writeShell, $bufferSize, $progressFunction, &$totalRead) {

            if ($writeShell->getStdInBufferFill() < $bufferSize) {
                $writeData = '';
                $readFunc = function ($data) use (&$writeData) {
                    $writeData = $data;
                };

                $success = $readShell->update(0.2, NULL, $pipeNumber == 1 ? $readFunc : NULL, $pipeNumber == 2 ? $readFunc : NULL);
                if ($success) {
                    $totalRead += strlen($writeData);
                    return $writeData;
                }
                return FALSE;
            }

        })) {

            if ($progressFunction && $lastTotalRead != $totalRead) {
                $lastTotalRead = $totalRead;
                $progressFunction($totalRead);
            }

            if (!$writeShell->isRunning() && $readShell->isRunning()) {
                throw new Shell\CommandException('Write process failed', $this);
            }

        }
    }


    protected function _openProcess(string $command, bool $sudo = FALSE) : void {
        $this->_assertProcessActive(FALSE);

        $this->_process = NULL;
        $this->_lastStatus = NULL;
        $this->_pipes = [NULL, NULL, NULL];
        $this->_stdInWriteBuffer = NULL;
        $this->_stdOutReadBuffer = '';
        $this->_stdErrReadBuffer = '';
        $this->_closeStdInAfterWrite = FALSE;

        if ($sudo) {
            $command = $this->_sudoizeCommand($command);
            $this->_stdInWriteBuffer = $this->_password . "\n";
        }

        $descriptorspec = [
            0 => ['pipe', 'r'], // STDIN
            1 => ['pipe', 'w'], // STDOUT
            2 => ['pipe', 'w']  // STDERR
        ];

        $this->_process = proc_open($command, $descriptorspec, $this->_pipes, NULL, $this->_env);

        if (!is_resource($this->_process)) {
            throw new Exception('Error opening process');
        }

        foreach ($this->_pipes as $pipe) {
            stream_set_blocking($pipe, FALSE);
            stream_set_read_buffer($pipe, 0);
            stream_set_write_buffer($pipe, 0);
        }
    }

    protected function _closeProcess(bool $assert = TRUE) : void {
        if ($assert) {
            $this->_assertProcessActive();
        }
        if (is_resource($this->_pipes[0])) {
            fclose($this->_pipes[0]);
        }
        if (is_resource($this->_pipes[1])) {
            fclose($this->_pipes[1]);
        }
        if (is_resource($this->_pipes[2])) {
            fclose($this->_pipes[2]);
        }

        while ($this->isRunning()) {
            usleep(10000);
        }
        if ($this->_process) {
            proc_close($this->_process);
        }
        $this->_process = NULL;
    }

    protected function _assertProcessActive(bool $status = TRUE) : void {
        if ($status && $this->_process === NULL) {
            throw new Exception('No process active');
        }
        if (!$status && $this->_process !== NULL) {
            throw new Exception('Process already running');
        }
    }

    protected function _sudoizeCommand(string $command) : string {
        if ($this->_password === NULL) {
            throw new Exception("Can't use 'sudo': no password set");
        }
        return "sudo -kS -p '' /bin/bash -c " . escapeshellarg($command);
    }

    public function setEnv(?array $env) : void {
        $this->_env = $env;
    }

    public function getEnv() : ?array {
        return $this->_env;
    }
}
