<?php

declare(strict_types = 1);

namespace Brightfish\Utility;

class ShellFS {


    /**
     * File exists and is block special
     */
    public const T_BLOCKSPECIAL = 'b';

    /**
     * File exists and is character special
     */
    public const T_CHARACTERSPECIAL = 'c';

    /**
     * File exists and is a directory
     */
    public const T_DIRECTORY = 'd';

    /**
     * File exists
     */
    public const T_EXISTS = 'e';

    /**
     * File exists and is a regular file
     */
    public const T_FILE = 'f';

    /**
     * File exists and is set-group-ID
     */
    public const T_SETGID = 'g';

    /**
     * File exists and is owned by the effective group ID
     */
    public const T_GROUPOWNED = 'G';

    /**
     * File exists and is a symbolic link
     */
    public const T_SYMLINK = 'L';

    /**
     * File exists and has its sticky bit set
     */
    public const T_STICKYBIT = 'k';

    /**
     * File exists and is owned by the effective user ID
     */
    public const T_USEROWNED = 'O';

    /**
     * File exists and is a named pipe
     */
    public const T_PIPE = 'p';

    /**
     * File exists and read permission is granted
     */
    public const T_READABLE = 'r';

    /**
     * File exists and has a size greater than zero
     */
    public const T_NONZERO = 's';

    /**
     * File exists and is a socket
     */
    public const T_SOCKET = 'S';

    /**
     * File exists and its set-user-ID bit is set
     */
    public const T_SETUID = 'u';

    /**
     * File exists and write permission is granted
     */
    public const T_WRITABLE = 'w';

    /**
     * File exists and execute (or search) permission is granted
     */
    public const T_EXECUTABLE = 'x';

    protected Shell $_shell;

    protected bool $_sudo;

    public function __construct(Shell $shell, bool $sudo = FALSE) {
        $this->_shell = $shell;
        $this->_sudo = $sudo;
    }

    /**
     * @param string $path
     *
     * @return string[]
     */
    public function ls(string $path) : array {
        if (!$this->_shell->runForExitCode('test -d ' . escapeshellarg($path), $this->_sudo)) {
            throw new \InvalidArgumentException("Path '$path' is not a directory");
        }

        $res = $this->_shell->run('ls -a ' . escapeshellarg($path), $this->_sudo);
        if ($res['exitcode'] != 0) {
            throw new \RuntimeException("Path '$path' can't be listed: " . trim($res['stderr']));
        }
        return array_diff(explode("\n", trim($res['stdout'])), ['.', '..']);
    }

    /**
     * @param string $path
     * @param array $tests
     *
     * @return bool
     */
    public function test(string $path, array $tests) : bool {
        $cmd = array_map(function ($t) use ($path) {
            return '-' . $t . ' ' . escapeshellarg($path);
        }, $tests);

        $cmdString = 'test ' . implode(' -a ', $cmd);

        return $this->_shell->runForExitCode($cmdString, $this->_sudo);
    }

    /**
     * @param string $path
     *
     * @return bool
     */
    public function isDirectory(string $path) : bool {
        return $this->test($path, [self::T_DIRECTORY]);
    }

    /**
     * @param string $path
     *
     * @return bool
     */
    public function isFile(string $path) : bool {
        return $this->test($path, [self::T_FILE]);
    }

    /**
     * @param string $path
     *
     * @return bool
     */
    public function isSymlink(string $path) : bool {
        return $this->test($path, [self::T_SYMLINK]);
    }

    /**
     * @param string $path
     *
     * @return bool
     */
    public function fileExists(string $path) : bool {
        return $this->test($path, [self::T_EXISTS]);
    }

    /**
     * @param string $path
     * @param string|NULL $mode
     *
     * @return void
     */
    public function mkdir(string $path, string $mode = NULL) : void {
        if ($this->fileExists($path)) {
            throw new \InvalidArgumentException("Path '$path' already exists");
        }
        if ($mode !== NULL) {
            $this->_validateModeString($mode);
        }

        $res = $this->_shell->run('mkdir ' . ($mode === NULL ? '' : '-m ' . escapeshellarg($mode) . ' ') . escapeshellarg($path), $this->_sudo);
        if ($res['exitcode'] != 0) {
            throw new \RuntimeException('Directory create of path "' . $path . '" failed: ' . trim($res['stderr']));
        }
    }

    /**
     * @param string $path
     * @param bool $recursive
     *
     * @return void
     */
    public function rm(string $path, bool $recursive = FALSE) : void {
        if (!$this->test($path, [self::T_SYMLINK])) {
            //don't perform a file-exists-check symlinks because it will fail if the target is already remove as part of a remove operation
            if (!$this->fileExists($path)) {
                throw new \InvalidArgumentException("Path '$path' does not exist");
            }
        }

        $res = $this->_shell->run('rm -f' . ($recursive ? 'r' : '') . ' ' . escapeshellarg($path), $this->_sudo);
        if ($res['exitcode'] != 0) {
            throw new \RuntimeException('Delete of path "' . $path . '" failed: ' . trim($res['stderr']));
        }
    }

    /**
     * @param string $path
     * @param string $mode
     * @param bool $recursive
     *
     * @return void
     */
    public function chmod(string $path, string $mode, bool $recursive = FALSE) : void {
        if (!$this->fileExists($path)) {
            throw new \InvalidArgumentException("Path '$path' does not exist");
        }
        $this->_validateModeString($mode);

        $res = $this->_shell->run('chmod ' . ($recursive ? '-R' : '') . ' ' . $mode . ' ' . escapeshellarg($path), $this->_sudo);
        if ($res['exitcode'] != 0) {
            throw new \RuntimeException('Mode change of path "' . $path . '" failed: ' . trim($res['stderr']));
        }
    }

    /**
     * @param string $path
     * @param string|NULL $user
     * @param string|NULL $group
     * @param bool $recursive
     *
     * @return void
     */
    public function chown(string $path, string $user = NULL, string $group = NULL, bool $recursive = FALSE) : void {
        if (!$this->fileExists($path)) {
            throw new \InvalidArgumentException("Path '$path' does not exist");
        }

        if ($user === NULL) {
            $user = '';
        } else {
            $this->_validateNameString($user);
        }

        if ($group === NULL) {
            $group = '';
        } else {
            $this->_validateNameString($group);
        }

        $res = $this->_shell->run('chown ' . ($recursive ? '-R' : '') . ' ' . $user . ':' . $group . ' ' . escapeshellarg($path), $this->_sudo);
        if ($res['exitcode'] != 0) {
            throw new \RuntimeException('Ownership change of path "' . $path . '" failed: ' . trim($res['stderr']));
        }
    }

    /**
     * @param string $path
     * @param string $content
     * @param string|NULL $mode
     *
     * @return void
     */
    public function mv(string $oldPath, string $newPath) : void {
        if (!$this->fileExists($oldPath)) {
            throw new \InvalidArgumentException("Path '$oldPath' does not exist");
        }

        $res = $this->_shell->run('mv ' . escapeshellarg($oldPath) . ' ' . escapeshellarg($newPath), $this->_sudo);
        if ($res['exitcode'] != 0) {
            throw new \RuntimeException('Move from "' . $oldPath . '" to "' . $newPath . '" failed: ' . trim($res['stderr']));
        }
    }

    /**
     * @param string $path
     * @param string $content
     * @param string|null $strMode The mode of the file, note that this will only be set if the file is newly created
     */
    public function writeFile(string $path, string $content, string $strMode = NULL) : void {

        $umaskPart = '';
        if ($strMode !== NULL) {

            $this->_validateModeString($strMode);

            $umaskPart = 'umask 0';

            $parts = str_split($strMode);
            if (count($parts) === 4) {
                array_shift($parts);
            }

            foreach ($parts as $part) {
                $umaskPart .= 7 - $part;
            }

            $umaskPart .= ' ; ';

        }

        $res = $this->_shell->run('(' . $umaskPart . 'cat - > ' . escapeshellarg($path) . ')', $this->_sudo, $content);
        if ($res['exitcode'] != 0) {
            throw new \RuntimeException('Write to path "' . $path . '" failed: ' . trim($res['stderr']));
        }

    }

    /**
     * @param string $path
     *
     * @return string
     */
    public function readFile(string $path) : string {
        if (!$this->test($path, [self::T_FILE, self::T_READABLE])) {
            throw new \InvalidArgumentException("Path '$path' is not a readable file");
        }

        $res = $this->_shell->run('cat ' . escapeshellarg($path), $this->_sudo);
        if ($res['exitcode'] != 0) {
            throw new \RuntimeException('File read from path "' . $path . '" failed: ' . trim($res['stderr']));
        }

        return $res['stdout'];
    }

    /**
     * @param string $localpath
     * @param string $remotepath
     * @param string|NULL $mode
     *
     * @return void
     */
    public function copyFileToRemote(string $localpath, string $remotepath, string $mode = NULL) : void {
        $this->writeFile($remotepath, file_get_contents($localpath), $mode);
    }

    /**
     * @param string $remotepath
     * @param string $localpath
     *
     * @return void
     */
    public function copyFileFromRemote(string $remotepath, string $localpath) : void {
        $content = $this->readFile($remotepath);
        file_put_contents($localpath, $content);
    }

    /**
     * @param string $mode
     *
     * @return void
     */
    private function _validateModeString(string $mode) : void {
        if (!preg_match('/^[0-7]{3,4}$/', $mode)) {
            throw new \InvalidArgumentException('Value ' . $mode . ' is not a valid mode string');
        }
    }

    /**
     * @param string $name
     *
     * @return void
     */
    private function _validateNameString(string $name) : void {
        if (!preg_match('/^[a-zA-Z0-9\-_\.]+$/', $name)) {
            throw new \InvalidArgumentException('Value ' . $name . ' is not a valid name');
        }
    }

}
