<?php

declare(strict_types = 1);

namespace Brightfish\Utility\Shell\tests;

use Brightfish\Utility\Shell;
use Brightfish\Utility\ShellFS;
use PHPUnit\Event\Runtime\PHP;
use PHPUnit\Event\Runtime\PHPUnit;
use PHPUnit\Framework\TestCase;

abstract class AShellTestBase extends TestCase {
    private readonly Shell $_bareShell;

    public function __construct(string $name) {
        parent::__construct($name);
        $this->_bareShell = new Shell();
    }

    public function getBareShell() : Shell {
        return $this->_bareShell;
    }

    public function createShellFS(Shell $shell) : ShellFS {
        return new ShellFS($shell);
    }
}
