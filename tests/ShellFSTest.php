<?php

declare(strict_types = 1);

namespace Brightfish\Utility\Shell\tests;

use Brightfish\Utility\Shell;
use Brightfish\Utility\ShellFS;

class ShellFSTest extends AShellTestBase {

    private static string $_TempDir;
    private readonly ShellFS $_shellFS;

    public function __construct(string $name) {
        parent::__construct($name);
        self::$_TempDir = sys_get_temp_dir();
        $this->_shellFS = $this->createShellFS($this->getBareShell());

    }

    public function getShellFS() : ShellFS {
        return $this->_shellFS;
    }

    public function getTempDir() : string {
        return self::$_TempDir;
    }

    public function testFSFunctionality() : void {
        $shellFS = $this->getShellFS();
        $this->assertInstanceOf(ShellFS::class, $shellFS);

        $dir = $this->getTempDir() . '/newDir';
        $shellFS->mkdir($dir);

        $this->assertTrue($shellFS->isDirectory($dir));

        $shellFS->writeFile($dir . '/test.txt', 'this is a test');
    }

    /**
     * @depends testFSFunctionality
     */
    public function testRmException() : void {
        $shellFS = $this->getShellFS();
        $this->expectException(\RuntimeException::class);
        $shellFS->rm($this->getTempDir() . '/newDir');
    }

    /**
     * @depends testRmException
     */
    public function testRmNewDir() : void {
        $dir = $this->getTempDir() . '/newDir';
        $this->getShellFS()->rm($dir, TRUE);
        $this->assertFalse($this->getShellFS()->isDirectory($dir));
    }

    public function testRmNotExisting() : void {
        $this->expectException(\InvalidArgumentException::class);
        $this->getShellFS()->rm($this->getTempDir() . '/890243729430254870579243259708759208709825720534');
    }

    public static function TearDownAfterClass() : void {
        if (is_dir(self::$_TempDir)) {
            $shell = new Shell();
            $fs = new ShellFS($shell);
            $fs->rm(self::$_TempDir, TRUE);
        }
    }
}
