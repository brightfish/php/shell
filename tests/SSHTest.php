<?php

declare(strict_types = 1);

namespace Brightfish\Utility\Shell\tests;

use Brightfish\Utility\Shell;
use Brightfish\Utility\Shell\SSH;

class SSHTest extends AShellTestBase {

    protected static string $_Host;

    private ?SSH\Interactive $_interactiveSSH = NULL;

    public function getInteractiveSSH() : SSH\Interactive {
        if ($this->_interactiveSSH === NULL) {
            $this->_interactiveSSH = new SSH\Interactive(self::$_Host);
        }
        return $this->_interactiveSSH;
    }
    public static function SetUpBeforeClass() : void {
        if ($_ENV['USER']) {
            Shell::SetUsername($_ENV['USER']);
        } else {
            Shell::SetCurrentUsername();
        }
        self::$_Host = $_ENV['SSH_HOST'] ?: 'unknown';
    }


    public function testSSHConnection() : void {
        $ssh = new SSH(self::$_Host);
        $this->assertInstanceOf(SSH::class, $ssh);

        $this->assertTrue($ssh->runForExitCode('ls'));
    }

    public function testInteractiveConnection() : void {
        $interactive = $this->getInteractiveSSH();
        $this->assertInstanceOf(SSH\Interactive::class, $interactive);

        $interactive->connect();

        $this->assertTrue($interactive->runForExitCode('ls'));

        $interactive->disconnect();
    }

    public function testConnectionNotRunning() : void {
        $interactive = $this->getInteractiveSSH();
        $this->expectException(Shell\Exception::class);
        $interactive->run('someCommand');
    }

    public function testInteractiveFS() : void {
        $interactive = $this->getInteractiveSSH();
        $interactive->connect();
        $shellFs = $this->createShellFS($interactive);

        $string = 'This is a test from phpunit on the project brightfish-bv/shell';
        $path = '/tmp/interactive-shell-fs-phpunit.txt';
        $shellFs->writeFile($path, $string);
        $this->assertEquals($string, $shellFs->readFile($path));
        $this->assertTrue($shellFs->isFile('/tmp/interactive-shell-fs-phpunit.txt'));

        $shellFs->rm('/tmp/interactive-shell-fs-phpunit.txt');

        $interactive->disconnect();
    }
}
