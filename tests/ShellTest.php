<?php

declare(strict_types = 1);

namespace Brightfish\Utility\Shell\tests;

use Brightfish\Utility\Shell;

class ShellTest extends AShellTestBase {

    public function testBasics() : void {
        $shell = $this->getBareShell();

        $this->assertInstanceOf(Shell::class, $shell);
        $this->assertEquals(TRUE, $shell->runForExitCode('ls -la'));
    }
}
